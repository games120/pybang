#!/usr/bin/python3

import os
import sys
import pygame
import time

# init

pygame.init()
screen = pygame.display.set_mode()
pygame.display.set_caption("PyBang")

screen_x, screen_y = screen.get_size()

texture_size_x = 300
texture_size_y = 200
screen_multiplier = 1

while texture_size_x * screen_multiplier < screen_x and texture_size_y * screen_multiplier < screen_y:
	screen_multiplier += 1
	
if not screen_multiplier == 1:
	screen_multiplier -= 1

screen = pygame.display.set_mode((texture_size_x * screen_multiplier, texture_size_y * screen_multiplier))


class PybangError(Exception):
	def __init__(self, message):
		super().__init__(message)


def mn(a):
	return a * screen_multiplier


def mp(x, y):
	return mn(x), mn(y)


# variables

game_directory = os.path.dirname(os.path.abspath(__file__))

fps = 60

click = 1

on_the_turn = "player_1"

connect_four_running = False
connect_four_paused = True
connect_four_winner = ""
connect_four_grid_points = [
	[0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0]
]

wait_until_intro_main = 60
show_intro_main = 0
intro_main = 0
hide_intro_main = 0
wait_until_start = 0

page = "intro_main"

# colors

white = (255, 255, 255)
grey = (30, 32, 34)
red = (255, 80, 80)
light_red = (180, 80, 80)
green = (80, 255, 80)
blue = (80, 80, 255)

# nodes

ubuntu5 = pygame.font.Font(game_directory + "/fonts/Ubuntu-M.ttf", mn(5))
ubuntu10 = pygame.font.Font(game_directory + "/fonts/Ubuntu-M.ttf", mn(10))

intro_text = ubuntu10.render("Made by EKNr1", True, white)
start_button_text = ubuntu5.render("Start", True, white)
settings_button_text = ubuntu5.render("Settings", True, white)
exit_button_text = ubuntu5.render("Exit", True, white)
select_mode_text = ubuntu10.render("Select Mode:", True, white)
connect_four_mode_button_text = ubuntu5.render("Connect Four", True, white)
player_one_is_on_the_turn_text = ubuntu5.render("Player 1 is on the turn!", True, blue)
player_two_is_on_the_turn_text = ubuntu5.render("Player 2 is on the turn!", True, green)
back_to_menu_button_text = ubuntu5.render("Back to menu", True, white)
player_one_has_won_the_game_text = ubuntu5.render("Player 1 has won the game!", True, blue)
player_two_has_won_the_game_text = ubuntu5.render("Player 2 has won the game!", True, green)

# functions


def exit_game():
	pygame.quit()
	print("Game closed.")
	sys.exit()


def drawgrid(width, height, blocksize, thickness):
	for x in range(width):
		for y in range(height):
			pygame.draw.rect(screen, white, pygame.Rect(x * blocksize, y * blocksize, blocksize, blocksize), thickness)


def reset_connect_four():
	global connect_four_grid_points
	global on_the_turn
	global connect_four_running
	global connect_four_paused

	connect_four_running = True
	connect_four_paused = False
	connect_four_grid_points = [
		[0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0]
	]

	on_the_turn = "player_1"


def detect_connect_four_input():
	global on_the_turn
	global click

	if connect_four_running:
		if pygame.mouse.get_pressed()[0]:
			if click == 0:
				for point in range(7):
					if cursor_x > point * mn(33) and cursor_x < point * mn(33) + mn(33):
						if cursor_y > 0 and cursor_y < mn(33):
							click = 1

							y = 0

							if on_the_turn == "player_1":
								while y < 6:
									if connect_four_grid_points[y][point] == 1 or connect_four_grid_points[y][point] == 2:
										break

									else:
										y += 1

								if not y == 0:
									y -= 1
									connect_four_grid_points[y][point] = 1

									on_the_turn = "player_2"

								detect_connect_four_win()

							elif on_the_turn == "player_2":
								while y < 6:
									if connect_four_grid_points[y][point] == 1 or connect_four_grid_points[y][point] == 2:
										break

									else:
										y += 1

								if not y == 0:
									y -= 1
									connect_four_grid_points[y][point] = 2

									on_the_turn = "player_1"

								detect_connect_four_win()

							else:
								raise PybangError("No player selected.")


def blit_chips():
	if connect_four_running:
		ya = 0
		for yb in connect_four_grid_points:
			xa = 0
			for xb in yb:
				if xb == 1:
					pygame.draw.circle(screen, blue, mp(xa * 33 + 16, ya * 33 + 16), mn(14))

				elif xb == 2:
					pygame.draw.circle(screen, green, mp(xa * 33 + 16, ya * 33 + 16), mn(14))

				xa += 1

			ya += 1


def detect_connect_four_win():
	y = -1
	for line in connect_four_grid_points:
		y += 1

		x = -1
		for chip in line:
			x += 1
			if not chip == 0:
				if x < 4:
					if line[x + 1] == chip and line[x + 2] == chip and line[x + 3] == chip:
						win(chip)
						return

				if y < 3:
					if connect_four_grid_points[y + 1][x] == chip and connect_four_grid_points[y + 2][x] == chip and connect_four_grid_points[y + 3][x] == chip:
						win(chip)
						return

				if x < 4 and y < 3:
					if connect_four_grid_points[y + 1][x + 1] == chip and connect_four_grid_points[y + 2][x + 2] == chip and connect_four_grid_points[y + 3][x + 3] == chip:
						win(chip)
						return

				if x > 2 and y < 3:
					if connect_four_grid_points[y + 1][x - 1] == chip and connect_four_grid_points[y + 2][x - 2] == chip and connect_four_grid_points[y + 3][x - 3] == chip:
						win(chip)
						return

def win(player):
	global connect_four_paused
	global connect_four_winner

	print("Player", player, "has won the game!")

	connect_four_winner = player
	connect_four_paused = True

# buttons

def button(xa, ya, xb, yb):
	global click

	if pygame.mouse.get_pressed()[0]:
		if click == 0:
			if cursor_x > xa - 1 and cursor_x < xb + 1:
				if cursor_y > ya - 1 and cursor_y < yb + 1:
					click = 1
					return True

				else:
					return False


def exit_button():
	pygame.draw.rect(screen, white, pygame.Rect(mn(142), mn(81), mn(15), mn(12)))
	pygame.draw.rect(screen, grey, pygame.Rect(mn(143), mn(82), mn(13), mn(10)))
	screen.blit(exit_button_text, mp(145, 84))

	if button(mn(142), mn(81), mn(157), mn(93)):
		exit_game()


def start_button():
	global page

	pygame.draw.rect(screen, white, pygame.Rect(mn(141), mn(47), mn(18), mn(12)))
	pygame.draw.rect(screen, grey, pygame.Rect(mn(142), mn(48), mn(16), mn(10)))
	screen.blit(start_button_text, mp(144, 50))

	if button(mn(141), mn(47), mn(159), mn(59)):
		page = "mode_select"


def connect_four_mode_button():
	global page
	global connect_four_grid_points

	pygame.draw.rect(screen, white, pygame.Rect(mn(132), mn(67), mn(37), mn(12)))
	pygame.draw.rect(screen, grey, pygame.Rect(mn(133), mn(68), mn(35), mn(10)))
	screen.blit(connect_four_mode_button_text, mp(135, 70))

	if button(mn(132), mn(67), mn(169), mn(79)):
		page = "connect_four"

		reset_connect_four()


def back_to_menu_button(x, y):
	global page

	pygame.draw.rect(screen, light_red, pygame.Rect(mn(x), mn(y), mn(37), mn(12)))
	pygame.draw.rect(screen, grey, pygame.Rect(mn(x + 1), mn(y + 1), mn(35), mn(10)))
	screen.blit(back_to_menu_button_text, mp(x + 3, y + 3))

	if button(x, y, mn(x + 37), mn(y + 12)):
		page = "main_menu"


# pages

def page_selector():
	if page == "intro_main":
		main_intro()

	if page == "main_menu":
		main_menu()

	if page == "mode_select":
		mode_select()

	if page == "connect_four":
		connect_four()


def main_intro():
	global wait_until_intro_main
	global show_intro_main
	global show_intro_main
	global intro_main
	global intro_main
	global hide_intro_main
	global hide_intro_main
	global page
	global wait_until_start
	
	if wait_until_intro_main > 0:
		wait_until_intro_main -= 1
	
		if wait_until_intro_main == 0:
			show_intro_main = 30
	
	if show_intro_main > 0:
		intro_text.set_alpha(255 - show_intro_main * 8.5)
		screen.blit(intro_text, (mn(300) / 2 - intro_text.get_width() / 2, mn(88)))
		show_intro_main -= 1
	
		if show_intro_main == 0:
			intro_main = 60
	
	if intro_main > 0:
		screen.blit(intro_text, (mn(300) / 2 - intro_text.get_width() / 2, mn(88)))
		intro_main -= 1
	
		if intro_main == 0:
			hide_intro_main = 30
		
	if hide_intro_main > 0:
		intro_text.set_alpha(hide_intro_main * 8.5)
		screen.blit(intro_text, (mn(300) / 2 - intro_text.get_width() / 2, mn(88)))
	
		hide_intro_main -= 1
	
		if hide_intro_main == 0:
			wait_until_start = 30
			
	if wait_until_start > 0:
		wait_until_start -= 1
		
		if wait_until_start == 0:
			page = "main_menu"


def main_menu():
	start_button()
	
	pygame.draw.rect(screen, white, pygame.Rect(mn(137), mn(64), mn(26), mn(12)))
	pygame.draw.rect(screen, grey, pygame.Rect(mn(138), mn(65), mn(24), mn(10)))
	screen.blit(settings_button_text, mp(140, 67))

	exit_button()


def mode_select():
	screen.blit(select_mode_text, mp(120, 30))

	connect_four_mode_button()


def connect_four():
	drawgrid(7, 6, mn(33), mn(1))

	if not connect_four_paused:
		detect_connect_four_input()

	blit_chips()

	back_to_menu_button(259, 184)

	if connect_four_paused:
		if connect_four_winner == 1:
			screen.blit(player_one_has_won_the_game_text, mp(233, 5))

		elif connect_four_winner == 2:
			screen.blit(player_two_has_won_the_game_text, mp(233, 5))

	else:
		if on_the_turn == "player_1":
			screen.blit(player_one_is_on_the_turn_text, mp(236, 5))

		elif on_the_turn == "player_2":
			screen.blit(player_two_is_on_the_turn_text, mp(236, 5))

# loop

while True:
	screen.fill(grey)
	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			exit_game()

	cursor_x, cursor_y = pygame.mouse.get_pos()
	if not pygame.mouse.get_pressed()[0]:
		click = 0

	page_selector()
	
	pygame.display.update()
	
	time.sleep(1 / fps)
